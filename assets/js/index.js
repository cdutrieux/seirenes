import icsToJson from 'ics-to-json'
import {DateTime} from 'luxon'

window.getEvents = async (icsUri) => {
  const icsRes = await fetch(icsUri);
  const icsData = await icsRes.text();
  // Convert
  const events = icsToJson(icsData);

  const now = DateTime.now();
  events.forEach(event => event.startDate = DateTime.fromISO(event.startDate)) 
  const futureEvents = events
	  .filter(event => event.startDate >  now)
	  .sort((a, b) => a.startDate > b.startDate);
  const pastEvents = events
	  .filter(event => event.startDate <=  now)
	  .sort((a, b) => a.startDate > b.startDate);
  return {
	  "futureEvents": futureEvents,
	  "pastEvents": pastEvents
  };
};
